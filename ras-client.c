#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <mysql/mysql.h>
#include "config.h"
#include "ras-events.h"
#include "ras-mc-handler.h"
#include "ras-aer-handler.h"
#include "ras-mce-handler.h"
#include "ras-logger.h"
#include "ras-client.h"


#define TIMESTAMP_LENGTH        (19)
#define HOST_NAME_MAX           (65)

int ras_mysql_client(struct ras_events *ras){
        ras->db_pub = calloc(1, sizeof(struct mysql_pub));
	ras_init_connection(ras->db_pub);
    
        return 0;
}

int ras_init_connection(struct mysql_pub *pub){
        if(mysql_library_init(0, NULL, NULL)) {
                log(TERM, LOG_INFO, "Unable to initialize MySQL Client Library");
        }
        if(!(pub->db = mysql_init(NULL))) {
                log(TERM, LOG_INFO, "Unable to intialize MySQL object");
        }
        if(!mysql_real_connect(pub->db, MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB, MYSQL_PORT, MYSQL_SOCKET, MYSQL_FLAGS)){
                log(TERM, LOG_INFO, "Unable to initialize MySQL database connection: %s", mysql_error(pub->db));
        } else {
                log(TERM, LOG_INFO, "Connection to %s as %s successful", MYSQL_HOST, MYSQL_USER);
        }

        return 0;
}

int ras_close_connection(struct mysql_pub *pub){
        mysql_close(pub->db);
        mysql_library_end();
        log(LOG_INFO, TERM, "Closing MySQL database connection...");

        return 0;
}

#define MC_INSERT_QUERY "INSERT INTO MC (id, hostname, timestamp, error_count, severity, msg, label, mc_index, top_layer, middle_layer, lower_layer, address, grain, syndrome, driver_detail, product) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
int ras_upload_mc_event(struct mysql_pub *pub, struct ras_mc_event *ev){
        pub->stmt_mc_event = mysql_stmt_init(pub->db);
        mysql_stmt_prepare(pub->stmt_mc_event, MC_INSERT_QUERY, strlen(MC_INSERT_QUERY));
        int ID = mysql_stmt_insert_id(pub->stmt_mc_event);
        char * hostname = calloc(HOST_NAME_MAX, sizeof(char));
        gethostname(hostname, (HOST_NAME_MAX - 1));
        FILE * mdi = fopen("/sys/class/dmi/id/product_family", "r");
        char * temp = calloc(12, sizeof(char));
        char * product = calloc(64, sizeof(char));
        if(mdi){
                while(fscanf(mdi, "%12s", temp) != EOF){
                        sprintf(product, "%s ", temp);
                }
        }

        MYSQL_BIND bind[16] = {0};
        
        bind[0].buffer_type = MYSQL_TYPE_LONG;
        bind[0].buffer = (int*) &ID;
        bind[0].buffer_length = sizeof(int);

        bind[1].buffer_type = MYSQL_TYPE_STRING;
        bind[1].buffer = hostname;
        bind[1].buffer_length = HOST_NAME_MAX;

        bind[2].buffer_type = MYSQL_TYPE_STRING;
        bind[2].buffer = ev->timestamp;
        bind[2].buffer_length = strlen(ev->timestamp);

        bind[3].buffer_type = MYSQL_TYPE_LONG;
        bind[3].buffer = (int*) &ev->error_count;
        bind[3].buffer_length = sizeof(int);

        bind[4].buffer_type = MYSQL_TYPE_STRING;
        bind[4].buffer = ev->error_type;
        bind[4].buffer_length = strlen(ev->error_type);

        bind[5].buffer_type = MYSQL_TYPE_STRING;
        bind[5].buffer = ev->msg;
        bind[5].buffer_length = strlen(ev->msg);

        bind[6].buffer_type = MYSQL_TYPE_STRING;
        bind[6].buffer = ev->label;
        bind[6].buffer_length = strlen(ev->label);

        bind[7].buffer_type = MYSQL_TYPE_BLOB;
        bind[7].buffer = (int*) &ev->mc_index;
        bind[7].buffer_length = sizeof(int);

        bind[8].buffer_type = MYSQL_TYPE_BLOB;
        bind[8].buffer = (signed char *) &ev->top_layer;
        bind[8].buffer_length = sizeof(char);

        bind[9].buffer_type = MYSQL_TYPE_BLOB;
        bind[9].buffer = (signed char *) &ev->middle_layer;
        bind[9].buffer_length = sizeof(char);

        bind[10].buffer_type = MYSQL_TYPE_BLOB;
        bind[10].buffer = (signed char *) &ev->lower_layer;
        bind[10].buffer_length = sizeof(char);

        bind[11].buffer_type = MYSQL_TYPE_BLOB;
        bind[11].buffer = (unsigned long long *) &ev->address;
        bind[11].buffer_length = sizeof(long long);

        bind[12].buffer_type = MYSQL_TYPE_BLOB;
        bind[12].buffer = (unsigned long long *) &ev->grain;
        bind[12].buffer_length = sizeof(long long);

        bind[13].buffer_type = MYSQL_TYPE_BLOB;
        bind[13].buffer = (unsigned long long *) &ev->syndrome;
        bind[13].buffer_length = sizeof(long long);

        
        bind[14].buffer_type = MYSQL_TYPE_STRING;
        bind[14].buffer = ev->driver_detail;
        bind[14].buffer_length = strlen(ev->driver_detail);

        bind[15].buffer_type = MYSQL_TYPE_STRING;
        bind[15].buffer = product;
        bind[15].buffer_length = strlen(product);

        mysql_stmt_bind_param(pub->stmt_mc_event, bind);
        mysql_stmt_execute(pub->stmt_mc_event);

        fclose(mdi);
        free(product);
        free(temp);
        free(hostname);
        return 0;

}

int ras_upload_mce_record(struct ras_events *ras, struct mce_event *ev){
        mysql_query(ras->db_pub->db, "INSERT INTO error (ID, name) VALUES (2, \"MCE\")");
        log(TERM, LOG_INFO, "%s", mysql_error(ras->db_pub->db));
        return 0;
}

#define NS_INSERT_QUERY "INSERT INTO Non_Standard (id, hostname, timestamp, sec_type, fru_id, fru_text, severity, error, product)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"

int ras_upload_ns_event(struct mysql_pub *pub, struct ras_non_standard_event *ev){
        pub->stmt_non_standard_record = mysql_stmt_init(pub->db);
        mysql_stmt_prepare(pub->stmt_non_standard_record, NS_INSERT_QUERY, strlen(NS_INSERT_QUERY));   
        FILE * mdi = fopen("/sys/class/dmi/id/product_family", "r");
        char * temp = calloc(12, sizeof(char));
        char * product = calloc(64, sizeof(char));
        if(mdi){
                while(fscanf(mdi, "%12s", temp) != EOF){
                        sprintf(product, "%s ", temp);
                }
        }
        int ID = mysql_stmt_insert_id(pub->stmt_non_standard_record);
        char * hostname = calloc(HOST_NAME_MAX, sizeof(char));
        gethostname(hostname, (HOST_NAME_MAX - 1));

        MYSQL_BIND bind[9] = {0};
        bind[0].buffer_type = MYSQL_TYPE_LONG;
        bind[0].buffer = &ID;
        bind[0].buffer_length = sizeof(int);
        
        bind[1].buffer_type = MYSQL_TYPE_STRING;
        bind[1].buffer = hostname;
        bind[1].buffer_length = HOST_NAME_MAX;

        bind[2].buffer_type = MYSQL_TYPE_STRING;
        bind[2].buffer = ev->timestamp;
        bind[2].buffer_length = TIMESTAMP_LENGTH;
      
        bind[3].buffer_type = MYSQL_TYPE_BLOB;
        bind[3].buffer = ev->sec_type;
        bind[3].buffer_length = 16;

        bind[4].buffer_type = MYSQL_TYPE_BLOB;
        bind[4].buffer = ev->fru_id;
        bind[4].buffer_length = 16;

        bind[5].buffer_type = MYSQL_TYPE_STRING;
        bind[5].buffer = ev->fru_text;
        bind[5].buffer_length = strlen(ev->fru_text);

        bind[6].buffer_type = MYSQL_TYPE_STRING;
        bind[6].buffer = ev->severity;
        bind[6].buffer_length = strlen(ev->severity);

        bind[7].buffer_type = MYSQL_TYPE_BLOB;
        bind[7].buffer = ev->error;
        bind[7].buffer_length = 16;

        bind[8].buffer_type = MYSQL_TYPE_STRING;
        bind[8].buffer = product;
        bind[8].buffer_length = strlen(product);


        /*bind[8].buffer_type = MYSQL_TYPE_LONG;
        bind[8].buffer = (uint32_t *) &ev->length;
        bind[8].buffer_length = sizeof(uint32_t);
        bind[8].is_null = 0; */

        mysql_stmt_bind_param(pub->stmt_non_standard_record, bind);
        mysql_stmt_execute(pub->stmt_non_standard_record);
        free(temp);
        free(product);
        free(hostname);
        fclose(mdi);
        return 0;

}

#define ARM_INSERT_QUERY "INSERT INTO ARM (id, hostname, timestamp, affinity, mpidr, midr, running_state, psci_state, product) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
int ras_upload_arm_record(struct mysql_pub *pub, struct ras_arm_event *ev){
        pub->stmt_arm_record = mysql_stmt_init(pub->db);
        mysql_stmt_prepare(pub->stmt_arm_record, ARM_INSERT_QUERY, strlen(ARM_INSERT_QUERY));   
        int ID = mysql_stmt_insert_id(pub->stmt_arm_record);
        char * hostname = calloc(HOST_NAME_MAX, sizeof(char));
        gethostname(hostname, (HOST_NAME_MAX - 1));
        MYSQL_BIND bind[9] = {0};
        FILE * mdi = fopen("/sys/class/dmi/id/product_family", "r");
        char * temp = calloc(12, sizeof(char));
        char * product = calloc(64, sizeof(char));
        if(mdi){
                while(fscanf(mdi, "%12s", temp) != EOF){
                        sprintf(product, "%s ", temp);
                }
        }

        bind[0].buffer_type = MYSQL_TYPE_LONG;
        bind[0].buffer = (int *) &ID;
        bind[0].buffer_length = sizeof(int);

        bind[1].buffer_type = MYSQL_TYPE_STRING;
        bind[1].buffer = hostname;
        bind[1].buffer_length = HOST_NAME_MAX;

        bind[2].buffer_type = MYSQL_TYPE_STRING;
        bind[2].buffer = (char *) &ev->timestamp;
        bind[2].buffer_length =  TIMESTAMP_LENGTH;

        bind[3].buffer_type = MYSQL_TYPE_BLOB;
        bind[3].buffer = (int8_t *) &ev->affinity;
        bind[3].buffer_length = sizeof(int8_t);

        bind[4].buffer_type = MYSQL_TYPE_BLOB;
        bind[4].buffer = (int64_t *) &ev->mpidr;
        bind[4].buffer_length = sizeof(int64_t);

        bind[5].buffer_type = MYSQL_TYPE_BLOB;
        bind[5].buffer = (int64_t *) &ev->midr;
        bind[5].buffer_length = sizeof(int64_t);

        bind[6].buffer_type = MYSQL_TYPE_BLOB;
        bind[6].buffer = (int32_t *) &ev->running_state;
        bind[6].buffer_length = sizeof(int32_t);

        bind[7].buffer_type = MYSQL_TYPE_BLOB;
        bind[7].buffer = (int32_t *) &ev->psci_state;
        bind[7].buffer_length = sizeof(int32_t);

        bind[8].buffer_type = MYSQL_TYPE_STRING;
        bind[8].buffer = product;
        bind[8].buffer_length = strlen(product);

        mysql_stmt_bind_param(pub->stmt_arm_record, bind);
        mysql_stmt_execute(pub->stmt_arm_record);
        log(LOG_INFO, TERM, "%s", mysql_stmt_error(pub->stmt_arm_record));
        
        fclose(mdi);
        free(product);
        free(temp);
        free(hostname);
        return 0;
}

#define AER_INSERT_QUERY "INSERT INTO AER (id, hostname, timestamp, severity, dev_name, tlp_header_valid, tlp_header, msg, product) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
int ras_upload_aer_event(struct mysql_pub * pub, struct ras_aer_event *ev){
        pub->stmt_aer_event = mysql_stmt_init(pub->db);
        mysql_stmt_prepare(pub->stmt_aer_event, AER_INSERT_QUERY, strlen(AER_INSERT_QUERY));
        int ID = mysql_stmt_insert_id(pub->stmt_aer_event);
        char * hostname = calloc(HOST_NAME_MAX, sizeof(char));
        gethostname(hostname, (HOST_NAME_MAX - 1));
        FILE * mdi = fopen("/sys/class/dmi/id/product_family", "r");
        char * temp = calloc(12, sizeof(char));
        char * product = calloc(64, sizeof(char));
        if(mdi){
                while(fscanf(mdi, "%12s", temp) != EOF){
                        sprintf(product, "%s ", temp);
                }
        }

        MYSQL_BIND bind[9] = {0};
        bind[0].buffer_type = MYSQL_TYPE_LONG;
        bind[0].buffer = (int *) &ID;
        bind[0].buffer_length = sizeof(int);

        bind[1].buffer_type = MYSQL_TYPE_STRING;
        bind[1].buffer = hostname;
        bind[1].buffer_length = HOST_NAME_MAX;

        bind[2].buffer_type = MYSQL_TYPE_STRING;
        bind[2].buffer = (char *) &ev->timestamp;
        bind[2].buffer_length =  TIMESTAMP_LENGTH;

        bind[3].buffer_type = MYSQL_TYPE_STRING;
        bind[3].buffer = ev->error_type;
        bind[3].buffer_length =  strlen(ev->error_type);

        bind[4].buffer_type = MYSQL_TYPE_STRING;
        bind[4].buffer = ev->dev_name;
        bind[4].buffer_length =  strlen(ev->dev_name);

        bind[5].buffer_type = MYSQL_TYPE_TINY;
        bind[5].buffer = (uint8_t *) &ev->tlp_header_valid;
        bind[5].buffer_length = sizeof(uint8_t);

        bind[6].buffer_type = MYSQL_TYPE_BLOB;
        bind[6].buffer = ev->tlp_header;
        bind[6].buffer_length = sizeof(uint32_t);

        bind[7].buffer_type = MYSQL_TYPE_STRING;
        bind[7].buffer = ev->msg;
        bind[7].buffer_length = strlen(ev->msg);

        bind[8].buffer_type = MYSQL_TYPE_STRING;
        bind[8].buffer = product;
        bind[8].buffer_length = strlen(product);

        mysql_stmt_bind_param(pub->stmt_aer_event, bind);
        mysql_stmt_execute(pub->stmt_aer_event);
        log(LOG_INFO, TERM, "%s", mysql_stmt_error(pub->stmt_aer_event));

        fclose(mdi);
        free(product);
        free(temp);
        free(hostname);
        return 0;
}

#define MF_INSERT_QUERY "INSERT INTO Memory_Failure (id, timestamp, pfn, page_type, action_result, product) VALUES (?, ?, ?, ?, ?, ?)"
int ras_upload_mf_event(struct mysql_pub *pub, struct ras_mf_event *ev){
        pub->stmt_mf_event = mysql_stmt_init(pub->db);
        mysql_stmt_prepare(pub->stmt_mf_event, MF_INSERT_QUERY, strlen(MF_INSERT_QUERY));
        int ID = mysql_stmt_insert_id(pub->stmt_mf_event);
        FILE * mdi = fopen("/sys/class/dmi/id/product_family", "r");
        char * temp = calloc(12, sizeof(char));
        char * product = calloc(64, sizeof(char));
        if(mdi){
                while(fscanf(mdi, "%12s", temp) != EOF){
                        sprintf(product, "%s ", temp);
                }
        }
        MYSQL_BIND bind[6] = {0};
        bind[0].buffer_type = MYSQL_TYPE_LONG;
        bind[0].buffer = (int *) &ID;
        bind[0].buffer_length = sizeof(int);

        bind[1].buffer_type = MYSQL_TYPE_STRING;
        bind[1].buffer = (char *) &ev->timestamp;
        bind[1].buffer_length =  TIMESTAMP_LENGTH;

        bind[2].buffer_type = MYSQL_TYPE_STRING;
        bind[2].buffer = (char *) &ev->pfn;
        bind[2].buffer_length =  30;

        bind[3].buffer_type = MYSQL_TYPE_STRING;
        bind[3].buffer = ev->page_type;
        bind[3].buffer_length =  strlen(ev->page_type);

        bind[4].buffer_type = MYSQL_TYPE_STRING;
        bind[4].buffer = ev->action_result;
        bind[4].buffer_length =  strlen(ev->action_result);

        bind[5].buffer_type = MYSQL_TYPE_STRING;
        bind[5].buffer = product;
        bind[5].buffer_length = strlen(product);

        mysql_stmt_bind_param(pub->stmt_mf_event, bind);
        mysql_stmt_execute(pub->stmt_mf_event);
        log(LOG_INFO, TERM, "%s", mysql_stmt_error(pub->stmt_mf_event));

        return 0;
}

#define DEVLINK_INSERT_QUERY "INSERT INTO Devlink (id, timestamp, bus_name, driver_name, reporter_name, msg, product) VALUES (?, ?, ?, ?, ?, ?, ?)"
int ras_upload_devlink_event(struct mysql_pub *pub, struct devlink_event *ev){
        pub->stmt_devlink_event = mysql_stmt_init(pub->db);
        mysql_stmt_prepare(pub->stmt_devlink_event, DEVLINK_INSERT_QUERY, strlen(DEVLINK_INSERT_QUERY));
        int ID = mysql_stmt_insert_id(pub->stmt_devlink_event);
        FILE * mdi = fopen("/sys/class/dmi/id/product_family", "r");
        char * temp = calloc(12, sizeof(char));
        char * product = calloc(64, sizeof(char));
        if(mdi){
                while(fscanf(mdi, "%12s", temp) != EOF){
                        sprintf(product, "%s ", temp);
                }
        }
        
        MYSQL_BIND bind[7] = {0};
        bind[0].buffer_type = MYSQL_TYPE_LONG;
        bind[0].buffer = (int *) &ID;
        bind[0].buffer_length = sizeof(int);

        bind[1].buffer_type = MYSQL_TYPE_STRING;
        bind[1].buffer = (char *) &ev->timestamp;
        bind[1].buffer_length =  TIMESTAMP_LENGTH;

        bind[2].buffer_type = MYSQL_TYPE_STRING;
        bind[2].buffer = ev->bus_name;
        bind[2].buffer_length =  strlen(ev->bus_name);

        bind[3].buffer_type = MYSQL_TYPE_STRING;
        bind[3].buffer = ev->driver_name;
        bind[3].buffer_length =  strlen(ev->driver_name);

        bind[4].buffer_type = MYSQL_TYPE_STRING;
        bind[4].buffer = ev->reporter_name;
        bind[4].buffer_length =  strlen(ev->reporter_name);

        bind[5].buffer_type = MYSQL_TYPE_STRING;
        bind[5].buffer = ev->msg;
        bind[5].buffer_length =  strlen(ev->msg);

        bind[6].buffer_type = MYSQL_TYPE_STRING;
        bind[6].buffer = product;
        bind[6].buffer_length = strlen(product);

        mysql_stmt_bind_param(pub->stmt_devlink_event, bind);
        mysql_stmt_execute(pub->stmt_devlink_event);
        log(LOG_INFO, TERM, "%s", mysql_stmt_error(pub->stmt_devlink_event));

        return 0;
}

#define DISKERROR_INSERT_QUERY "INSERT INTO Disk_Error (id, timestamp, dev, sector, nr_sector, error, rwbs, cmd, product) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
int ras_upload_diskerror_event(struct mysql_pub *pub, struct diskerror_event *ev){
        pub->stmt_diskerror_event = mysql_stmt_init(pub->db);
        mysql_stmt_prepare(pub->stmt_diskerror_event, DISKERROR_INSERT_QUERY, strlen(DISKERROR_INSERT_QUERY));
        int ID = mysql_stmt_insert_id(pub->stmt_diskerror_event);
        FILE * mdi = fopen("/sys/class/dmi/id/product_family", "r");
        char * temp = calloc(12, sizeof(char));
        char * product = calloc(64, sizeof(char));
        if(mdi){
                while(fscanf(mdi, "%12s", temp) != EOF){
                        sprintf(product, "%s ", temp);
                }
        }

        MYSQL_BIND bind[10] = {0};
        bind[0].buffer_type = MYSQL_TYPE_LONG;
        bind[0].buffer = (int *) &ID;
        bind[0].buffer_length = sizeof(int);

        bind[1].buffer_type = MYSQL_TYPE_STRING;
        bind[1].buffer = (char *) &ev->timestamp;
        bind[1].buffer_length =  TIMESTAMP_LENGTH;

        bind[2].buffer_type = MYSQL_TYPE_STRING;
        bind[2].buffer = ev->dev;
        bind[2].buffer_length = sizeof(char);

        bind[3].buffer_type = MYSQL_TYPE_LONGLONG;
        bind[3].buffer = (unsigned long long *) &ev->sector;
        bind[3].buffer_length = sizeof(long long);

        bind[4].buffer_type = MYSQL_TYPE_LONG;
        bind[4].buffer = (unsigned int *) &ev->nr_sector;
        bind[4].buffer_length = sizeof(int);

        bind[5].buffer_type = MYSQL_TYPE_STRING;
        bind[5].buffer = ev->error;
        bind[5].buffer_length =  strlen(ev->error);

        bind[7].buffer_type = MYSQL_TYPE_STRING;
        bind[7].buffer = ev->rwbs;
        bind[7].buffer_length = strlen(ev->rwbs);

        bind[8].buffer_type = MYSQL_TYPE_STRING;
        bind[8].buffer = ev->cmd;
        bind[8].buffer_length = strlen(ev->cmd);

        bind[9].buffer_type = MYSQL_TYPE_STRING;
        bind[9].buffer = product;
        bind[9].buffer_length = strlen(product);

        mysql_stmt_bind_param(pub->stmt_diskerror_event, bind);
        mysql_stmt_execute(pub->stmt_diskerror_event);
        log(LOG_INFO, TERM, "%s", mysql_stmt_error(pub->stmt_diskerror_event));

        return 0;
}