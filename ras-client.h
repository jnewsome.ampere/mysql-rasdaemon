#ifndef __RAS_CLIENT_H
#define __RAS_CLIENT_H

#include <stdint.h>
#include "config.h"


#ifdef HAVE_MYSQL

#include <mysql/mysql.h>

struct mysql_pub {
	MYSQL *db;
	MYSQL_STMT *stmt_mc_event;
#ifdef HAVE_AER
	MYSQL_STMT	*stmt_aer_event;
#endif
#ifdef HAVE_MCE
	MYSQL_STMT	*stmt_mce_record;
#endif
#ifdef HAVE_EXTLOG
	MYSQL_STMT	*stmt_extlog_record;
#endif
#ifdef HAVE_NON_STANDARD
	MYSQL_STMT	*stmt_non_standard_record;
#endif
#ifdef HAVE_ARM
	MYSQL_STMT	*stmt_arm_record;
#endif
#ifdef HAVE_DEVLINK
	MYSQL_STMT	*stmt_devlink_event;
#endif
#ifdef HAVE_DISKERROR
	MYSQL_STMT	*stmt_diskerror_event;
#endif
#ifdef HAVE_MEMORY_FAILURE
	MYSQL_STMT	*stmt_mf_event;
#endif
};

struct mysql_pub;

int ras_mysql_client(struct ras_events *ras);
int ras_init_connection(struct mysql_pub *pub);
int ras_close_connection(struct mysql_pub *pub);
int ras_upload_mc_event(struct mysql_pub *pub, struct ras_mc_event *ev);
int ras_upload_mce_record(struct ras_events *ras, struct mce_event *ev);
int ras_upload_ns_event(struct mysql_pub *pub, struct ras_non_standard_event *ev);
int ras_upload_arm_record(struct mysql_pub *pub, struct ras_arm_event *ev);
int ras_upload_aer_event(struct mysql_pub *pub, struct ras_aer_event *ev);
int ras_upload_mf_event(struct mysql_pub *pub, struct ras_mf_event *ev);
int ras_upload_devlink_event(struct mysql_pub *pub, struct devlink_event *ev);
int ras_upload_diskerror_event(struct mysql_pub *pub, struct diskerror_event *ev);

#endif
#endif
